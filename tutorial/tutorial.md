# ENZO/RAMSES Setup on Deepthought2

Enzo and Ramses are cosmological simulation codes. This tutorial is an explination on how to get it setup on deepthought2, supercomputer at UMD.

## Installation

### Set up enviornment
If you want to install on your home computer then you will need several software packages. Deepthougth2 has all the necessary packages pre-installed. However you must load these pacakges. This is done by typing:

### RAMSES
First you will need to clone the code from the bitbucket repository of RAMSES. Go to the directory where you want to store the code and on the terminal type:

	$ git clone https://bitbucket.org/rteyssie/ramses.git

Next move to the directory that has all the RAMSES Makefiles. On the terminal type:

	$ cd trunk/ramses/bin/

You will see there are three different Makefiles. One is standard RAMSES which is the basic hydro solver. There are two more, one is for radiative transfer and the other is for radiative hydrodynamics. 


### Enzo
First you will need to clone the code from the bitbucket repository of ENZO. Go to the directory where you want to store the code and on the terminal type:

	$ hg clone https://bitbucket.org/enzo/enzo-dev

Next move to the directory that has all the enzo source files. On the terminal type:

	$ cd enzo/src/enzo

First you will need to change your machine name to deepthought2. On the terminal type:

	$ vim Make.config.machine

When the file comes up change it so that it says:

	CONFIG_MACHINE = deepthought2

Afterwords, you need to create a parameter file, where you specify the cosmological constants, box size and other parameters that ENZO uses.

Before you compile code on deepthought2 you must load the appropriate compilers. You can find a chart of the compiler combinations 

Once you have your initial condition file and your parameter file to run the simulation you need to use the command:
	
	<compiler> -n <# of processors> ./<enzo_exe>.exe -d <parameter_file>.param >& output.log

In order to run this on deepthougth2 you must submit a job, which is described below. 

### MUSIC
Music is not a cosmological simulation but an initial condition generator. This is necessary when running cosmological simulations. In order to start working with music you need to setup a configuration file. This is where you setup all the parameters for the simulation.

## Submitting Jobs
In order to actually run simualtions you will need to submit jobs so the job manager can allocate resources and actually run your simulation 
