# README #

This is a project on the comparison on two different cosomological simulations (ENZO and RAMSES). 

### How do I get set up? ###

* There is a tutorial in the tutorial folder on how to set up MUSIC, ENZO and RAMSES on deepthought2 at UMD. 
* Files that are specific for this project are in the src folder.

### Who do I talk to? ###

* Rajath Shetty (rajathks@outlook.com)